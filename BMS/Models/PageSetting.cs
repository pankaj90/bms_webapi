﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BMS.Models
{
    public class PageSetting
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string WhereClause { get; set; }
        public string OrderBy { get; set; }
        public bool IsAscending { get; set; }
    }
    public static class PagingUtils
    {
        public static IEnumerable<T> Page<T>(this IEnumerable<T> en, int pageSize, int page)
        {
            return en.Skip((page - 1) * pageSize).Take(pageSize);
        }
        public static IQueryable<T> Page<T>(this IQueryable<T> en, int pageSize, int page)
        {
            return en.Skip((page - 1) * pageSize).Take(pageSize);
        }
    }
    public class Response
    {
        public string Message { get; set; }
        public object Result { get; set; }
        public int StatusId { get; set; }
        public static Response CreateResponse(string message, object data, int statusId)
        {
            return new Response()
            {
                Message = message,
                Result = data,
                StatusId = statusId
            };
        }
    }

    public class PagingResponse
    {
        public string Message { get; set; }
        public object Result { get; set; }
        public Int32 TotalRecords { get; set; }
        public static PagingResponse CreateResponse(string message, object data, Int32 totalRecords)
        {
            return new PagingResponse()
            {
                Message = message,
                Result = data,
                TotalRecords = totalRecords
            };
        }
    }
    public class DeleteEntity
    {
        public int Id { get; set; }
        public TableType TableType { get; set; }
    }

    public enum TableType
    {
        BookCategory = 1,
        Book = 2
    }
}
