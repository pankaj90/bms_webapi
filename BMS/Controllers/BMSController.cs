﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using BMS.Models;
using System.Data;

namespace BMS.Controllers
{
    public class BMSController : ApiController
    {
        BMSEntities entity = null;
        BMSController()
        {
            entity = new BMSEntities();
        }

        [HttpPost]
        [ActionName("GetBooksCategories")]
        public PagingResponse GetBooksCategories([FromUri]PageSetting pageSetting)
        {
            var result = PagingUtils.Page(entity.Books.Where(x => (x.Status ?? true)
                  && (string.IsNullOrEmpty(pageSetting.WhereClause) ? true :
                    (
                        x.BookCode.ToLower().Contains(pageSetting.WhereClause)
                     || x.BookTitle.ToLower().Contains(pageSetting.WhereClause)
                    ))
                )
                .OrderByDescending(x => x.ID), pageSetting.PageSize, pageSetting.PageNumber)
                .Select(x => new
                {
                    Id = x.ID,
                    BookCode = x.BookCode,
                    BookTitle = x.BookTitle
                }).ToList();
            var totalRecords = entity.Books.Where(x => (x.Status ?? true)
            && (string.IsNullOrEmpty(pageSetting.WhereClause) ? true :
                    (
                        x.BookCode.ToLower().Contains(pageSetting.WhereClause)
                     || x.BookTitle.ToLower().Contains(pageSetting.WhereClause)
                    ))
            ).Count();
            return PagingResponse.CreateResponse("Data retreive successfully", result, totalRecords);
        }

        /// <summary>
        /// Delete Entity
        /// </summary>
        /// <param name="tableEntity"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("Delete")]
        public Response Delete([FromUri]DeleteEntity tableEntity)
        {
            try
            {
                switch (tableEntity.TableType)
                {
                    case TableType.BookCategory:
                        var category = entity.Books.Where(x => x.ID == tableEntity.Id).FirstOrDefault();
                        category.Status = false;
                        entity.Entry(category).State = EntityState.Modified;
                        entity.SaveChanges();
                        break;
                    case TableType.Book:
                        var state = entity.BookFields.Where(x => x.ID == tableEntity.Id).FirstOrDefault();
                        state.Status = false;
                        entity.Entry(state).State = EntityState.Modified;
                        entity.SaveChanges();
                        break;
                }
                return Response.CreateResponse("Entity deleted successfully.", 1, 1);
            }
            catch (Exception ex)
            {
                return Response.CreateResponse(ex.Message, 0, 0);
            }
        }

        /// <summary>
        /// Save Category
        /// </summary>
        /// <param name="pageSetting"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("SaveCategory")]
        public Response SaveCategory([FromUri]Book book)
        {
            Book existing = new Book();
            if (book.ID > 0)
            {
                existing = entity.Books.Where(x => x.ID == book.ID).FirstOrDefault();
            }
            var isDuplicate = entity.Books.Where(x => x.BookCode.ToLower() == book.BookCode.ToLower() && x.Status != false
                && (book.ID == 0 ? true : x.ID != book.ID))
                .FirstOrDefault();
            if (isDuplicate != null)
            {
                return Response.CreateResponse("Can't add duplicate record.", book, 0);
            }
            try
            {
                if (book.ID > 0)
                {
                    existing.BookCode = book.BookCode;
                    existing.BookTitle = book.BookTitle;
                    existing.ModifiedDate = DateTime.Now;
                    entity.Entry(existing).State = EntityState.Modified;
                    entity.SaveChanges();
                    return Response.CreateResponse("Category updated successfull.", book, 1);
                }
                book.CreatedDate = DateTime.Now;
                book.ModifiedDate = book.CreatedDate;
                book.Status = true;
                book.CreatedBy = 1;
                entity.Books.Add(book);
                entity.SaveChanges();
                return Response.CreateResponse("Category saved successfull.", book, 1);
            }
            catch (Exception ex)
            {
                return Response.CreateResponse("An error occurred.", book, 0);
            }
        }

        [HttpPost]
        public Response GetEntityDetail([FromUri]DeleteEntity tableEntity)
        {
            try
            {
                object obj = new object();
                switch (tableEntity.TableType)
                {
                    case TableType.BookCategory:
                        obj = entity.Books.Where(x => x.ID == tableEntity.Id && x.Status == true)
                            .Select(x => new
                            {
                                CategoryCode = x.BookCode,
                                CategoryName = x.BookTitle,
                                Id = x.ID
                            })
                            .FirstOrDefault();
                        break;
                    //case TableType.BookCategory:
                    //    obj = entity.States.Where(x => x.StateId == tableEntity.Id && x.IsDeleted != true)
                    //        .Select(x => new StateData()
                    //        {
                    //            StateCode = x.StateCode,
                    //            StateId = x.StateId,
                    //            CountryId = x.CountryId,
                    //            StateName = x.StateName
                    //        })
                    //        .FirstOrDefault();

                    //    break;
                    //case TableType.City:
                    //    obj = entity.Cities.Where(x => x.CityId == tableEntity.Id && x.IsDeleted != true)
                    //            .Select(x => new CityData()
                    //            {
                    //                CityId = x.CityId,
                    //                CityName = x.CityName,
                    //                CityCode = x.CityCode,
                    //                StateId = x.StateId,
                    //                CountryId = x.State.CountryId
                    //            })
                    //            .FirstOrDefault();
                    //    break;
                }
                return Response.CreateResponse("Entity retreived successfully.", obj, 1);
            }
            catch (Exception ex)
            {
                return Response.CreateResponse(ex.Message, 0, 0);
            }
        }

    }
}
